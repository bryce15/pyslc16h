#!/usr/bin/env python3
# slc16hir.py - Python3 control software for certain scrolling LED displays
# with a serial port. The one from which I reverse engineered this was a "128"
# type, 4" x 26" "industrial grade" sign that cost about $100. It has an RJ-11
# jack that is a serial port, and a 5V 3A power supply. The nominal model 
# number could be SLC16H-IR or M500N-7X80RG2 and also has an IR remote control.
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.




# A line-beginning 0x0X does various transition modes, e.g. starting
# with 0x02 is immediate and 0x03 is scrolling conventionally.
# 0x16 is flashing. 
# 0xEF 0xxx control codes:
# 0x5x - nothing
# 0x6x - various character graphics e.g. 0x64 is telephone
# 0x7x - various character graphics e.g. 0x79 is a diagonal up arrow, 7c - shoe
# 0x80 - clock
# 0x81 - date
# 0x82 - unknown "0.0"
# 0x9x - various canned animations
# 0xA0 - Smallest font (5 high)
# 0xA1 - Small bold
# 0xA2 - Normal font
# 0xA3 - Wider font
# 0xA4 - Bolt font
# 0xA5 - Large bold
# 0xA6 - Compact font (6 high)
# 0xb0 - red
# 0xb1 - bright red
# 0xb2 - orange
# 0xb3 - lime green
# 0xb4 - yellow
# 0xb5 - bright yellow
# 0xb6 - bright green
# 0xb7 - bright green
# 0xb8 - mixed dots
# 0xb9 - horizontal bands of color
# 0xbA - mixed red orange
# 0xBB - mixed green red
# 0xBC - Green on red
# 0xBD - red on green
# 0xBE - Yellow on red
# 0xBF - Orange on green
# 0xC8 and above seem to create progressively longer pauses. 0xC9 is 2 sec.
# 0xC4-C6 make flickering
# 0xC7 is very slow scrolling.
# 0xDx are graphics. e.g. 0xdd is a container ship and 0xde is swimmers.
# 0xEx is beeps. 0xEF is obviously special. 0xE7 is a nice repeated beep.
# 0xE1 is a phone ring, 0xE0 is quick beeps, 0xE2 and some others are single.

import serial

class Color:
    dim_red = b'\xEF\xB0'
    red = b'\xEF\xB1'
    orange = b'\xEF\xB2'
    lime_green = b'\xEF\xB3'
    dim_yellow = b'\xEF\xB4'
    yellow = b'\xEF\xB5'
    dim_green = b'\xEF\xB6'
    green = b'\xEF\xB7'
    mixed_dots = b'\xEF\xB8'
    bands = b'\xEF\xB9'
    red_orange = b'\xEF\xBA'
    green_red = b'\xEF\xBB'
    green_on_red = b'\xEF\xBC'
    red_on_green = b'\xEF\xBD'
    yellow_on_green = b'\xEF\xBE'
    orange_on_green = b'\xEF\xBF'

class Pause:
    two_seconds = b'\xEF\xC9'
    three_seconds = b'\xEF\xCA'
    four_seconds = b'\xEF\xCB'
    six_seconds = b'\xEF\xCC'
    ten_seconds = b'\xEF\xCD'
    twenty_seconds = b'\xEF\xCE'
    thirty_seconds = b'\xEF\xCF'

class Font:
    smaller = b'\xEF\xA0'
    small_bold = b'\xEF\xA1'
    normal = b'\xEF\xA2'
    wide = b'\xEF\xA3'
    bold = b'\xEF\xA4'
    wide_bold = b'\xEF\xA5'
    small = b'\xEF\xA6'

class Beeps:
    ring = b'\xEF\xE1'
    single = b'\xEF\xE2'
    repeat = b'\xEF\xE7'

class Message:
    _prefix = b''
    def __init__(self, *texts):
        self._text = []
        for text in texts: self(text)
    def _render(self):
        return self._prefix + b''.join(self._text)
    def __call__(self, *texts):
        for text in texts:
            self._text.append(
                bytes(text, 'ascii') if isinstance(text, str) else text)


class MsgCyclic(Message):
    _prefix = b'\x01'
class MsgImmediate(Message):
    _prefix = b'\x02'
class MsgRightOpen(Message):
    _prefix = b'\x03'
class MsgLeftOpen(Message):
    _prefix = b'\x04'
class MsgCenterOutOpen(Message):
    _prefix = b'\x05'
class MsgCenterInOpen(Message):
    _prefix = b'\x06'
class MsgCenterOutCover(Message):
    _prefix = b'\x07'
class MsgRightCover(Message):
    _prefix = b'\x08'
class MsgLeftCover(Message):
    _prefix = b'\x09'
class MsgCenterInCover(Message):
    _prefix = b'\x0A'
class MsgScrollUp(Message):
    _prefix = b'\x0B'
class MsgScrollDown(Message):
    _prefix = b'\x0C'
class MsgInterlaceCenter(Message):
    _prefix = b'\x0D'
class MsgInterlaceCover(Message):
    _prefix = b'\x0E'
class MsgCoverUp(Message):
    _prefix = b'\x0F'
class MsgCoverDown(Message):
    _prefix = b'\x10'
class MsgScanLine(Message):
    _prefix = b'\x11'
class MsgExplode(Message):
    _prefix = b'\x12'
class MsgPacMan(Message):
    _prefix = b'\x13'
class MsgFallStack(Message):
    _prefix = b'\x14'
class MsgShoot(Message):
    _prefix = b'\x15'
class MsgFlash(Message):
    _prefix = b'\x16'
class MsgRandom(Message):
    _prefix = b'\x17'
class MsgSlideIn(Message):
    _prefix = b'\x18'

class Sign:
    def __init__(self, port, filename=b"01"):
        """Create a new sign object. Port is an object with a "write"
           method that will send a bytes instance, or else a string in 
           which case it is to be the name of a device pyserial can open."""
        self._port = port if hasattr(port, "write") else serial.Serial(
                port, baudrate=2400)
        self._filename = filename

    def send(self, *messages):
        """Send one or more messages to the sign. The message can be a string
           or bytes object, a Message object, or a list of either."""
        self._init_seq()
        #if not isinstance(messages, list): messages = [messages]
        for n,message in enumerate(messages): 
            self._port.write(message._render() if isinstance(message, Message
                ) else bytes(message))
            if n == len(messages) - 1:
                self._port.write(b"\xFF\xFF\x00")
            else:
                self._port.write(b"\xFF")

    def _init_seq(self):
        self._port.write(b"\x00\xff\xff\x00\x0b")
        self._port.write(bytes(range(0x80)))
        self._port.write(b"\xFF\x01"+self._filename)


# Example
if __name__ == '__main__':
    sign = Sign("/dev/ttyUSB0")
    message = MsgScanLine()
    message(Color.red, "RED", Color.green, "GREEN")
    message2 = MsgFlash()
    message2(Color.orange, "ORANGE", Color.yellow, "YELLOW")

    sign.send(MsgImmediate(Font.smaller, Color.yellow, "Small"), 
          MsgImmediate(Font.small_bold, Beeps.ring, Color.red, "Small bold"),
          MsgImmediate(Font.normal, Color.lime_green, "Normal"),
          MsgImmediate(Font.wide, Color.green, "Wide"),
          MsgImmediate(Font.bold, Beeps.repeat, Color.red, "Bold"),
          MsgImmediate(Font.wide_bold, Color.green, "Wide", Color.bands,
                        " bold"),
          MsgImmediate(Font.small, Beeps.single, Color.red_on_green,
              "Small"),
          MsgFlash(Font.smaller, Color.yellow, "",
                                     Color.green_on_red, "  ", 
                                     Color.yellow, " ALERT! ",
                                     Color.green_on_red, "  ",
                                     Color.yellow, "",
                                     Beeps.repeat, Beeps.repeat,
                                     Pause.two_seconds),
          MsgImmediate(Font.smaller, Color.yellow, "",
                                     Color.green_on_red, "  ", 
                                     Color.yellow, " ALERT! ",
                                     Color.green_on_red, "  ",
                                     Color.yellow, "",
                                     Pause.six_seconds)
          )
